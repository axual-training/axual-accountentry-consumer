package io.axual.training.consumer;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicBoolean;

import io.axual.payments.AccountEntryEvent;
import io.axual.payments.AccountId;
import io.axual.training.common.consumer.ConsumerProperties;

@Component
public class SimpleRecordConsumer implements InitializingBean {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleRecordConsumer.class);
    private Consumer<AccountId, AccountEntryEvent> consumer = null;

    private final AtomicBoolean isInitialized;

    public SimpleRecordConsumer(@Autowired ConsumerProperties consumerProperties) {
        LOG.info("Creating Kafka Consumer with properties {}", consumerProperties.asProperties());
        consumer = new KafkaConsumer(consumerProperties.asProperties());
        isInitialized = new AtomicBoolean(false);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // Initialize the consumer
        isInitialized.set(true);
    }

    @Scheduled(fixedRate = 500)
    public void run() {
        if (isInitialized.get()) {
            /*
                This method is called every 500 milliseconds.

                Call the poll method to read from the topic
                more info: https://kafka.apache.org/20/javadoc/org/apache/kafka/clients/consumer/KafkaConsumer.html
                You can compile this application with the command: mvn compile package
                You can run this application with the command: mvn spring-boot:run
             */
        }
    }

}
